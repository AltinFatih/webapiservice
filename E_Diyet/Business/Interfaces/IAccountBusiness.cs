﻿using E_Diyet.Entities.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace E_Diyet.Business.Interfaces
{
    public interface IAccountBusiness
    {
        int InsertUser(Users insusers);
        Users SelectUsersByUsername(string username);
    }
}
