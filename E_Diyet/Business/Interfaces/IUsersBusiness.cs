﻿using E_Diyet.Entities.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace E_Diyet.Business.Interfaces
{
    public interface IUsersBusiness
    {
        Users SelectUsersByUsername(string userName);
        int InsertErrors(ErrorLog errors);
        bool UpdateUsersFailedAttempt(Users users);
        void UpdateUsersSuccessAttempts(Users users, UserLoginHistory userLoginHistory);
    
        List<string> SelectRoles();
        //int InsertRoles(Roles roles);
    }
}
