﻿using E_Diyet.Business.Interfaces;
using E_Diyet.DataAccess;
using E_Diyet.DataAccess.Interfaces;
using E_Diyet.Entities.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace E_Diyet.Business.Services
{
    public class AccountBusiness : IAccountBusiness
    {
        private readonly IAccountData _accountData;
        public AccountBusiness(IAccountData accountData)
        {
            _accountData = accountData;
        }
        public int InsertUser(Users insusers)
        {
            DBConnector connector = new DBConnector();
            return _accountData.InsertUser(insusers, connector);
        }

        public Users SelectUsersByUsername(string username)
        {
            DBConnector connector = new DBConnector();

            return _accountData.SelectUsersByUsername(username, connector);
        }
    }
}
