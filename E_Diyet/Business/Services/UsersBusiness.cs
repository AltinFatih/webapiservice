﻿using Docker.DotNet.Models;
using E_Diyet.Business.Interfaces;
using E_Diyet.DataAccess;
using E_Diyet.DataAccess.Interfaces;
using E_Diyet.Entities.Entity;
using E_Diyet.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace E_Diyet.Business.Services
{
    public class UsersBusiness : IUsersBusiness
    {
        private readonly IUsersData _usersData;
        public UsersBusiness(IUsersData usersData)
        {
            _usersData = usersData;
        }

        public int InsertErrors(ErrorLog errors)
        {
            DBConnector connector = new DBConnector();
            return _usersData.InsertErrors(errors,connector);
          
        }

        public int InsertUserLoginHistory(UserLoginHistory userLoginHistory)
        {
            DBConnector connector = new DBConnector();
            return _usersData.InsertUserLoginHistory(userLoginHistory, connector);
        }

        public List<string> SelectRoles()
        {
            DBConnector connector = new DBConnector();

            return _usersData.SelectRoles(connector);
        }

        public Users SelectUsersByUsername(string userName)
        {
            DBConnector connector = new DBConnector(); 

            return _usersData.SelectUsersByUsername(userName,connector);
        }

        public bool UpdateUsersFailedAttempt(Users users)
        {
            DBConnector connector = new DBConnector();
            try
            {
                connector.BeginTransaction();

                _usersData.UpdateUsersAccessFailedCountByID(users.AccessFailedCount, users.Id,connector);

                if (users.AccessFailedCount == ConfigValues.MaxFailedAccessLockout)
                {
                    users.IsLockedOut = true;
                    _usersData.IsLockedOut(users.IsLockedOut,users.Id,connector);
                }

                connector.CommitTransaction();
                return true;
            }
            catch (Exception)
            {
                connector.RollbackTransaction();
                throw;
            }

        }

        public void UpdateUsersSuccessAttempts(Users users, UserLoginHistory userLoginHistory)
        {
            DBConnector connector = new DBConnector();
            try
            {
                connector.BeginTransaction();

                _usersData.UpdateUsersSuccessLoginByID(users,connector);
                _usersData.InsertUserLoginHistory(userLoginHistory, connector);

                connector.CommitTransaction();
            }
            catch (Exception)
            {
                connector.RollbackTransaction();
                throw;
            }
        }
    }
}
