﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using E_Diyet.Entities.Request;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System.IdentityModel.Tokens.Jwt;
using E_Diyet.TokenManagers;
using E_Diyet.Enum;
using E_Diyet.Business.Interfaces;
using E_Diyet.Entities.Entity;
using E_Diyet.Models;
using E_Diyet.Util;
using Microsoft.AspNetCore.Identity;

namespace E_Diyet.Controllers
{
    // create user, role tablosuna kayıt atacaksın. admin rolünü


    [Route("[controller]")]
    [ApiController]
    public class AuthenticationController : ApiBaseController<AuthenticationController>
    {
        private readonly IUsersBusiness _usersBusiness;

        public AuthenticationController(IUsersBusiness usersBusiness)
        {
            _usersBusiness = usersBusiness;
        }

        /// <summary>
        /// Kullanıcı girişi yapar bla bla
        /// </summary>
        /// <param name="loginRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("UserLogin")]
        public Response UserLogin([FromBody] LoginRequest loginRequest)
        {
            Users users = _usersBusiness.SelectUsersByUsername(loginRequest.Username);

            if (!UserControl(users, out string errorCode, out string errorMessage))
            {
                return Error(errorCode, errorMessage);
            }

            if (users.Password != loginRequest.Password)
            {
                users.AccessFailedCount += 1;
                _usersBusiness.UpdateUsersFailedAttempt(users);
            }

            // role listini db den çekmek gerekir
            //List<string> roleList = new List<string>
            //{ "Admin"
            //};




            //Roles roles = new Roles()
            //{
            //    Name = loginRequest.Name,
            //    Description = "",
            //    IsActive = false,
            //    CreatedOn = DateTime.Now
            //};
            //_usersBusiness.InsertRoles(roles);
            //List<Roles> roleList = new List<Roles>();
            //roleList.Add(new Roles
            //{
            //    Name = loginRequest.Name
            //});

            List<string> roleList = _usersBusiness.SelectRoles();

            Token token = new Token();
            if (!token.GetToken(Guid.NewGuid(), roleList, out errorMessage, out string tokenData))
            {
                return Error(errorMessage);
            }
            else
            {
                // adamın lockedout false ve accessfailedcount u 0 a çekeceksin.
                // users tablosunda lastlogindate ve lastloginIp alanlarını güncelleyeceksin.
                users.AccessFailedCount = 0;
                users.IsLockedOut = false;
                users.LastLoginIp = UserHostAddress;
                users.LastLoginDate = DateTime.Now;

                // user login historye kayıt atacaksın.
                UserLoginHistory userLoginHistory = new UserLoginHistory()
                {
                    UserID = users.Id,
                    UserAgent = UserAgent,
                    ClientIp = UserHostAddress,
                    LoginDate = DateTime.Now
                };

                _usersBusiness.UpdateUsersSuccessAttempts(users, userLoginHistory);

                Dictionary<string, object> data = new Dictionary<string, object>
                {
                    { "Token", tokenData },
                    { "NameSurname", users.Name + string.Empty + users.SurName },
                  
                };
                return Success(data);
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="users"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        private bool UserControl(Users users, out string errorCode, out string errorMessage)
        {
            if (users == null)
            {
                errorCode = "DYT0001";
                errorMessage = Utils.GetErrorMessageByErrorCode(errorCode);
                return false;
            }

            if (users.IsLockedOut)
            {
                errorCode = "DYT0002";
                errorMessage = Utils.GetErrorMessageByErrorCode(errorCode);
                return false;
            }
            if (!users.Status)
            {
                errorCode = "DYT0003";
                errorMessage = Utils.GetErrorMessageByErrorCode(errorCode);
                return false;
            }

            errorMessage = string.Empty;
            errorCode = string.Empty;
            return true;
        }
    }
}