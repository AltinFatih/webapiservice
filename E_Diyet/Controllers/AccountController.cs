﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using E_Diyet.Entities.Request;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System.IdentityModel.Tokens.Jwt;
using E_Diyet.TokenManagers;
using E_Diyet.Enum;
using E_Diyet.Business.Interfaces;
using E_Diyet.Entities.Entity;
using E_Diyet.Models;
using E_Diyet.Util;
using Microsoft.AspNetCore.Identity;

namespace E_Diyet.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class AccountController : ApiBaseController<AccountController>
    {

        private readonly IAccountBusiness _accountBusiness;

        public AccountController(IAccountBusiness accountBusiness)
        {
            _accountBusiness = accountBusiness;

        }

        [HttpPost]
        [Route("Register")]
        public Response Register([FromBody] RegisterRequest registerRequest)
        {

            Users users = _accountBusiness.SelectUsersByUsername(registerRequest.Username);

            if (!UserControl(users, out string errorCode, out string errorMessage))
            {
                return Error(errorCode, errorMessage);
            }


            Users insusers = new Users()
            {
                UserId = Guid.NewGuid(),
                Email = registerRequest.Username,
                UserName = registerRequest.Username,
                Name = registerRequest.Name,
                SurName = registerRequest.SurName,
                Password = registerRequest.Password,
                AccessFailedCount = 0,
                IsLockedOut = false,
                Status = true,
                LastLoginIp = UserHostAddress,
                //LastLoginDate = DateTime.Now,
                Createdon = DateTime.Now
            };




            _accountBusiness.InsertUser(insusers);

            Dictionary<string, object> data = new Dictionary<string, object>
                {

                    { "NameSurname", insusers.Name + string.Empty + insusers.SurName }
                };
            return Success(data);

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="users"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        private bool UserControl(Users users, out string errorCode, out string errorMessage)
        {
            if (users != null)
            {
                errorCode = "DYT0004";
                errorMessage = Utils.GetErrorMessageByErrorCode(errorCode);
                return false;
            }


            errorMessage = string.Empty;
            errorCode = string.Empty;
            return true;
        }
    }
}
