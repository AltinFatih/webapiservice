﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using E_Diyet.Models;
using E_Diyet.Util;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json;

namespace E_Diyet.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ApiBaseController<T> : Controller
    {
        public string UserHostAddress => _currentApiRequest.UserHostAddress;
        public string FunctionPath => _currentApiRequest.ApplicationPath;
        public string UserAgent => _currentApiRequest.UserAgent;
        public Guid UserId => _currentApiRequest.UserId;
        public string RequestParameters => _currentApiRequest.PostParameters;

        private ApiRequest _currentApiRequest;
        public override void OnActionExecuting(ActionExecutingContext controllerContext)
        {
            #region Try to parse incoming data from client

            _currentApiRequest = new ApiRequest();

            #endregion

            #region Fill in other information from Request object

            _currentApiRequest.ApplicationPath = controllerContext.HttpContext.Request.Path;
            _currentApiRequest.UserAgent = controllerContext.HttpContext.Request.Headers["User-Agent"].ToString();
            _currentApiRequest.PostParameters = JsonConvert.SerializeObject(controllerContext.ActionArguments);
            _currentApiRequest.UserHostAddress = GetRemoteIPAddress(controllerContext.HttpContext);

            if (controllerContext.HttpContext.User.Identity.IsAuthenticated)
            {
                string value = (controllerContext.HttpContext.User.Identity as ClaimsIdentity).Claims.First(claim => claim.Type == "UID").Value;

                _currentApiRequest.UserId = Guid.Parse(value);
            }

            #endregion

            base.OnActionExecuting(controllerContext);
        }
        public string GetRemoteIPAddress(HttpContext context, bool allowForwarded = true)
        {
            if (allowForwarded)
            {
                string header = context.Request.Headers["CF-Connecting-IP"].FirstOrDefault() ?? context.Request.Headers["X-Forwarded-For"].FirstOrDefault();

                if (System.Net.IPAddress.TryParse(header, out IPAddress ip))
                {
                    return ip.ToString();
                }
            }
            return context.Connection.RemoteIpAddress.ToString();
        }
        public Response Success()
        {
            return Success(null);
        }

        public Response Success(Dictionary<string, object> responseData)
        {
            Response apiResponse = new Response
            {
                ErrorMessage = string.Empty,
                IsSuccessful = true,
                ResponseData = responseData
            };

            return apiResponse;
        }

        public Response Error(string errorMessage)
        {
            return Error("", errorMessage);
        }
        public Response Error(string errorCode, string errorMessage)
        {
            return Error(errorCode, errorMessage, null);
        }
        /// <summary>
        /// Apo Log 
        /// </summary>
        /// <param name="errorCode"></param>
        /// <param name="errorMessage"></param>
        /// <param name="exception"></param>
        /// <returns></returns>
        public Response Error(string errorCode, string errorMessage, Exception exception)
        {
            Response apiResponse = new Response
            {
                ErrorCode = errorCode,
                ErrorMessage = errorMessage,
                IsSuccessful = false,
                ResponseData = null
            };
            // TODO : Error log tablosuna kayıt atılacak.
            return apiResponse;
        }

        [Serializable]
        public class ApiRequest
        {
            public ApiRequest() { }
            public Guid UserId { get; set; }
            public string ApplicationPath { get; set; }
            public string MethodName { get; set; }
            public string UserAgent { get; set; }
            public string UserHostAddress { get; set; }
            public string Headers { get; set; }
            public string AppRelativeCurrentExecutionFilePath { get; set; }
            public string UserHostName { get; set; }
            public string PostParameters { get; set; }
        }
    }
}