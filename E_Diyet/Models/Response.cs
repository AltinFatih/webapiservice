﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace E_Diyet.Models
{
    public class Response
    {
        public bool IsSuccessful { get; set; }
        public string ErrorMessage { get; set; }
        public string ErrorCode { get; set; }
        public int ErrorType { get; set; }
        public Dictionary<string, object> ResponseData { get; set; }
    }
}
