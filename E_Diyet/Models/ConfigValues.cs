﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace E_Diyet.Models
{
    public class ConfigValues
    {
        public static string SecretKey { get; set; }
        public static int ExpireMinutes { get; set; }
        public static string ConnectionString { get; set; }
        public static int MaxFailedAccessLockout { get; set; }
    }
}
