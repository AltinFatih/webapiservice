﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace E_Diyet.Models
{
    public class JWTContainerModel : IAuthContainerModel
    {
        #region Public Methods
        public int ExpireMinutes { get; set; } = ConfigValues.ExpireMinutes;
        public string SecretKey { get; set; } = ConfigValues.SecretKey;
        public string SecurityAlgorithm { get; set; } = SecurityAlgorithms.HmacSha256Signature;
        public string Issuer { get; set; } = "localhost";
        public string Audience { get; set; } = "https://www.setcard.com.tr/";
        public Claim[] Claims { get; set; }

        #endregion
    }
}
