﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text.Json.Serialization;

namespace E_Diyet.Entities.Entity
{
    public class UserLoginHistory
    {
        public int Id { get; set; }
        public int UserID { get; set; }
        public string UserAgent { get; set; }
        public string ClientIp { get; set; }
        public DateTime LoginDate { get; set; }
    }
}
