﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text.Json.Serialization;

namespace E_Diyet.Entities.Entity
{
    public class ErrorLog
    {
        public int Id { get; set; }
        public int ErrorId { get; set; }
        public string ErrorMassage { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}
