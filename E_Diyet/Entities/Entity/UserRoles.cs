﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text.Json.Serialization;

namespace E_Diyet.Entities.Entity
{
    public class UserRoles
    {
        public int Id { get; set; }
        public int RoleID { get; set; }
        public int UserID { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}
