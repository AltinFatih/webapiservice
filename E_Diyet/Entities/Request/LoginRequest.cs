﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace E_Diyet.Entities.Request
{
    public class LoginRequest
    {
        /// <summary>
        /// Email
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "Lütfen Email adresinizi giriniz.")]
        [EmailAddress(ErrorMessage = "Lütfen Email adresinizi doğru girdiğinizden emin olunuz.")]
        public string Username { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "Lütfen şifrenizi giriniz")]
        [RegularExpression("^(?=.*[a-zğüşöç])(?=.*[A-ZİĞÜŞÖÇ])(?=.*\\d)(?=.*[@$.;\\-_<>#{}!%*?&])[A-Za-zğüşöçİĞÜŞÖÇ\\d@$.;\\-_<>#{}!%*?&]{8,}$", ErrorMessage = "Şifreniz minimum 8 karakterden oluşmalı, en az 1 küçük harf, en az 1 büyük harf, en az 1 rakam ve 1 özel karakter içermelidir.")]
        public string Password { get; set; }
        public string Name { get; set; }
    }
}
