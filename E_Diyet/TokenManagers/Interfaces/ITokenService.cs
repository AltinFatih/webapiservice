﻿using E_Diyet.Models;
using System.Collections.Generic;
using System.Security.Claims;

namespace E_Diyet.TokenManagers.Interfaces
{
    public interface ITokenService
    {
        string SecretKey { get; set; }
        bool IsTokenValid(string token);
        string GenerateToken(IAuthContainerModel model);
        IEnumerable<Claim> GetTokenClaims(string token);
    }
}
