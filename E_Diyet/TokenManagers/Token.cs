﻿using E_Diyet.TokenManagers;
using E_Diyet.Models;
using E_Diyet.TokenManagers.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using E_Diyet.TokenManagers.Interfaces;
using E_Diyet.Entities.Entity;

namespace E_Diyet.TokenManagers
{
    public class Token
    {
        /// <summary>
        /// Get token 
        /// </summary>
        /// <param name="UserId"></param>
        /// <param name="errorMessage"></param>
        /// <param name="tokenOut"></param>
        /// <returns></returns>
        public bool GetToken(Guid UserId, List<string> roleList, out string errorMessage, out string tokenOut)
        {
            try
            {
                IAuthContainerModel model = GetJWTContainerModel(UserId, roleList);
                ITokenService authService = new TokenService(model.SecretKey);

                string token = authService.GenerateToken(model);

                if (!authService.IsTokenValid(token))
                {
                    errorMessage = "Giriş yapılamadı";
                    tokenOut = string.Empty;
                    return false;
                }
                else
                {
                    errorMessage = string.Empty;
                    tokenOut = token;
                    return true;
                }

            }
            catch (Exception ex)
            {
                errorMessage = ex.ToString();
                tokenOut = string.Empty;
                return false;
            }

        }
        /// <summary>
        /// Create JWT Container Model
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        private static JWTContainerModel GetJWTContainerModel(Guid userId, List<string> roleList)
        {
            List<Claim> claims = new List<Claim>
            {
                new Claim("UID", userId.ToString()),
                new Claim("App", "E-Diyet")
            };

            foreach (var role in roleList)
            {
                claims.Add(new Claim("role", role));
            }

            return new JWTContainerModel()
            {
                Claims = claims.ToArray(),

            };
        }

      
    }
}
