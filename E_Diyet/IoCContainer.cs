﻿using Autofac;
using E_Diyet.Business.Interfaces;
using E_Diyet.Business.Services;
using E_Diyet.DataAccess.Interfaces;
using E_Diyet.DataAccess.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace E_Diyet
{
    public class IoCContainer : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);
            builder.RegisterType<UsersBusiness>().As<IUsersBusiness>().InstancePerLifetimeScope();
            builder.RegisterType<UsersRepo>().As<IUsersData>().InstancePerLifetimeScope();
            builder.RegisterType<AccountBusiness>().As<IAccountBusiness>().InstancePerLifetimeScope();
            builder.RegisterType<AccountRepo>().As<IAccountData>().InstancePerLifetimeScope();
        }
    }
}
