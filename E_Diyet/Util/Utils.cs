﻿using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace E_Diyet.Util
{
    public class Utils
    {
        private static readonly MemoryCache _memCache = new MemoryCache(new MemoryCacheOptions());

        #region Hata mesajları dönme
        public static string GetErrorMessageByErrorCode(string errorCode)
        {
            if (!(_memCache.Get("ErrorList") is Dictionary<string, string> errorList) || errorList.Count < 1)
            {
                string json = File.ReadAllText("./errorcodes.json");

                errorList = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);

                _memCache.Set("ErrorList", errorCode);
            }

            errorList.TryGetValue(errorCode, out string errorMessage);
            return errorMessage;
        }
        #endregion
    }
}
