﻿using Dapper;
using E_Diyet.DataAccess.Interfaces;
using E_Diyet.Entities.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace E_Diyet.DataAccess.Repository
{
    public class UsersRepo : IUsersData
    {
        public int InsertErrors(ErrorLog errors, DBConnector connector)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("@CreatedOn", errors.CreatedOn);
            parameters.Add("@ErrorMessage", errors.ErrorMassage);
            parameters.Add("@ErrorId", errors.ErrorId);

            return connector.InsertEntity(Queries.Queries.InsertErrors, parameters);
        }

        public int InsertUserLoginHistory(UserLoginHistory userLoginHistory, DBConnector connector)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("@UserID", userLoginHistory.UserID);
            parameters.Add("@UserAgent", userLoginHistory.UserAgent);
            parameters.Add("@ClientIp", userLoginHistory.ClientIp);
            parameters.Add("@LoginDate", userLoginHistory.LoginDate);

            return connector.InsertUserLoginHistory(Queries.Queries.InsertUserLoginHistory, parameters);
        }

        public bool IsLockedOut(bool ısLockedOut, int ıd, DBConnector connector)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("@Id", ıd);
            parameters.Add("@IsLockedOut", ısLockedOut);

            return connector.UpdateEntity(Queries.Queries.IsLockedOut, parameters);
        }

        public Users SelectUsersByUsername(string userName, DBConnector connector)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("@UserName", userName);

            return connector.SelectEntityWithParameters<Users>(Queries.Queries.SelectUsersByUsername, parameters);
        }
        public List<string> SelectRoles( DBConnector connector)
        {

                 return connector.SelectList<string>(Queries.Queries.SelectRoles);

        }
        public bool UpdateUsersSuccessLoginByID(Users users, DBConnector connector)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("@Id", users.Id);
            parameters.Add("@AccessFailedCount", users.AccessFailedCount);
            parameters.Add("@IsLockedOut", users.IsLockedOut);
            parameters.Add("@LastLoginIp", users.LastLoginIp);
            parameters.Add("@LastLoginDate", users.LastLoginDate);
            
            return connector.UpdateEntity(Queries.Queries.UpdateUsersSuccessLoginByID, parameters);
        }

        public bool UpdateUsersAccessFailedCountByID(short accessFailedCount, int ıd, DBConnector connector)
        {

            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("@Id", ıd);
            parameters.Add("@AccessFailedCount", accessFailedCount);

            return connector.UpdateEntity(Queries.Queries.UpdateUsersAccessFailedCountByID, parameters);
        }

       
    }
}
