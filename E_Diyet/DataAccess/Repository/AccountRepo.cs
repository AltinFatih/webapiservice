﻿using Dapper;
using E_Diyet.DataAccess.Interfaces;
using E_Diyet.Entities.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace E_Diyet.DataAccess.Repository
{
    public class AccountRepo : IAccountData
    {

        public int InsertUser(Users insusers, DBConnector connector)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("@UserId", insusers.UserId);
            parameters.Add("@Email", insusers.Email);
            parameters.Add("@UserName", insusers.UserName);
            parameters.Add("@Name", insusers.Name);
            parameters.Add("@SurName", insusers.SurName);
            parameters.Add("@Password", insusers.Password);
            parameters.Add("@AccessFailedCount", insusers.AccessFailedCount);
            parameters.Add("@IsLockedOut", insusers.IsLockedOut);
            parameters.Add("@Status", insusers.Status);
            parameters.Add("@LastLoginIp", insusers.LastLoginIp);
            //parameters.Add("@LastLoginDate", insusers.LastLoginDate);
            parameters.Add("@Createdon", insusers.Createdon);


            return connector.InsertEntity(Queries.Queries.InsertUsers, parameters);
        }

        public Users SelectUsersByUsername(object userName, DBConnector connector)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("@UserName", userName);

            return connector.SelectEntityWithParameters<Users>(Queries.Queries.SelectUsersByUsername, parameters);
        }
    }
}
