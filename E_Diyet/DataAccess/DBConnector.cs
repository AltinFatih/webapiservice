﻿using Dapper;
using E_Diyet.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace E_Diyet.DataAccess
{
    public class DBConnector
    {
        // look isolation type
        private IsolationLevel IsolationType { get; set; } = IsolationLevel.ReadUncommitted;

        private string _connectionString;
        public IDbConnection _dbConnection { get; set; } = null;

        public IDbTransaction _dbTransaction { get; set; } = null;

        public DBConnector()
        {
            _connectionString = ConfigValues.ConnectionString;
        }
        #region preparation for database operations  
        // open _dbConnection
        public void OpenConnection()
        {

            if (_dbConnection == null)
            {
                _dbConnection = new SqlConnection(_connectionString);
            }

            if (_dbConnection.State == ConnectionState.Closed)
            {
                try
                {
                    _dbConnection.Open();
                }
                catch (Exception ex)
                {
                    throw ex;
                }

            }
        }

       

        // close _dbConnection
        public void CloseConnection()
        {
            if (_dbTransaction == null)
            {
                if (_dbConnection != null)
                {
                    try
                    {
                        _dbConnection.Close();
                    }
                    catch (Exception ex)
                    {

                        throw ex;
                    }

                    _dbConnection.Dispose();
                    _dbConnection = null;
                }
            }
        }
        public void BeginTransaction()
        {
            if (_dbConnection == null || _dbConnection.State == ConnectionState.Closed)
            {
                this.OpenConnection();
            }
            if (_dbTransaction == null)
            {
                try
                {
                    _dbTransaction = _dbConnection.BeginTransaction(IsolationType);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        public void CommitTransaction()
        {
            if (_dbTransaction != null)
            {
                try
                {
                    _dbTransaction.Commit();
                }
                catch (Exception ex)
                {

                    throw ex;
                }

                _dbTransaction = null;
                this.CloseConnection();
            }
        }
        public void RollbackTransaction()
        {
            if (_dbTransaction != null)
            {
                try
                {
                    _dbTransaction.Rollback();
                }
                catch (Exception ex)
                {

                    throw ex;
                }

                _dbTransaction = null;
                this.CloseConnection();
            }
        }
        #endregion

        #region sync List Methods
        // Get All List with sp
        public List<T> SelectList<T>(string query) where T : class
        {
            this.OpenConnection();
            var result = (_dbConnection.Query<T>(query)).ToList();
            this.CloseConnection();
            return result;

        }

        // Get All List with sp and parameters
        public List<T> SelectListWithParameters<T>(string query, object parameters) where T : class
        {
            this.OpenConnection();
            var result = (_dbConnection.Query<T>(query, parameters)).ToList();
            this.CloseConnection();
            return result.ToList();

        }

        public List<T> SelectListWithParametersSP<T>(string query, object parameters) where T : class
        {
            this.OpenConnection();
            var result = (_dbConnection.Query<T>(query, parameters, commandType: CommandType.StoredProcedure)).ToList();
            this.CloseConnection();
            return result.ToList();

        }
        #endregion

        #region Get Entity By parameters (like by Id)

        // Get Entity , methods will be return one row
        public async Task<T> AsyncSelectEntityWithParameters<T>(string query, object parameters) where T : class
        {
            this.OpenConnection();
            var result = await _dbConnection.QueryFirstAsync<T>(query, parameters);
            this.CloseConnection();
            return result;

        }
        public T SelectEntityWithParameters<T>(string query, object parameters) where T : class
        {
            this.OpenConnection();
            var result = _dbConnection.Query<T>(query, parameters).FirstOrDefault();
            this.CloseConnection();
            return result;

        }
        public T SelectEntityWithParametersSP<T>(string query, object parameters) where T : class
        {
            this.OpenConnection();
            var result = _dbConnection.Query<T>(query, parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();
            this.CloseConnection();
            return result;

        }

        #endregion

        #region Update Entity
        // Update Entity , method return unique id
        public async Task<bool> AsyncUpdateEntity(string query, object parameters)
        {
            bool isSucces;
            this.OpenConnection();
            try
            {
                await _dbConnection.ExecuteAsync(query, parameters);
                isSucces = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            this.CloseConnection();
            return isSucces;

        }
        public bool UpdateEntity(string query, object parameters)
        {
            bool isSucces;
            this.OpenConnection();
            try
            {
                int result = _dbConnection.Execute(query, parameters, transaction: _dbTransaction);
                if (result > 0)
                    isSucces = true;
                else
                    isSucces = false;

                this.CloseConnection();
            }
            catch (Exception ex)
            {
                this.CloseConnection();
                throw ex;
            }
            return isSucces;

        }
        public bool UpdateEntitySP(string query, object parameters)
        {
            bool isSucces;
            this.OpenConnection();
            try
            {
                _dbConnection.Execute(query, parameters, transaction: _dbTransaction, commandType: CommandType.StoredProcedure);
                isSucces = true;
            }
            catch (Exception ex)
            {
                this.CloseConnection();
                throw ex;
            }

            this.CloseConnection();
            return isSucces;

        }
        #endregion
        public dynamic ExecuteSP(string query, DynamicParameters parameters)
        {
            this.OpenConnection();
            try
            {
                var result = _dbConnection.Execute(query, parameters, commandType: CommandType.StoredProcedure);
                this.CloseConnection();
                return result;
            }
            catch (Exception ex)
            {
                this.CloseConnection();
                throw ex;
            }
        }
        public int InsertEntity(string query, object parameters)
        {
            this.OpenConnection();
            try
            {
                var result = _dbConnection.Query<int>(query, parameters, transaction: _dbTransaction, commandTimeout: 120).Single();
                this.CloseConnection();
                return result;
            }
            catch (Exception ex)
            {
                this.CloseConnection();
                throw ex;
            }

        }
        internal int InsertUserLoginHistory(string query, DynamicParameters parameters)
        {
            this.OpenConnection();
            try
            {
                var result = _dbConnection.Query<int>(query, parameters, transaction: _dbTransaction, commandTimeout: 120).Single();
                this.CloseConnection();
                return result;
            }
            catch (Exception ex)
            {
                this.CloseConnection();
                throw ex;
            }
        }
       
    }
}
