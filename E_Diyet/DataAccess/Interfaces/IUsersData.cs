﻿using E_Diyet.Entities.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace E_Diyet.DataAccess.Interfaces
{
    public interface IUsersData
    {
        Users SelectUsersByUsername(string userName,DBConnector connector);
        int InsertErrors(ErrorLog errors, DBConnector connector);
        int InsertUserLoginHistory(UserLoginHistory userLoginHistory, DBConnector connector);
        bool UpdateUsersAccessFailedCountByID(short accessFailedCount, int ıd, DBConnector connector);
        bool IsLockedOut(bool ısLockedOut, int ıd, DBConnector connector);
        bool UpdateUsersSuccessLoginByID(Users users, DBConnector connector);
        List<string> SelectRoles( DBConnector connector);
    }
}
