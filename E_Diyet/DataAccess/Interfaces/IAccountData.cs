﻿using E_Diyet.Entities.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace E_Diyet.DataAccess.Interfaces
{
    public interface IAccountData
    {
        int InsertUser(Users insusers, DBConnector connector);
        Users SelectUsersByUsername(object userName, DBConnector connector);
    }
}
