﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace E_Diyet.DataAccess.Queries
{
    public class Queries
    {
        #region Insert Queries

        public const string InsertUsers = @"INSERT INTO [dbo].[Users]([UserID],[Email],[UserName],[Name],[SurName],[Password],[AccessFailedCount],[IsLockedOut],[Status],[LastLoginIp],[Createdon])
                                              VALUES(@UserID,@Email ,@UserName,@Name,@SurName,@Password ,@AccessFailedCount,@IsLockedOut,@Status,@LastLoginIp,@Createdon); SELECT CAST(SCOPE_IDENTITY() as int)";
        public const string InsertErrors = @"INSERT INTO [dbo].[Errors]([ErrorId],[ErrorMassage],[CreatedOn])
                                              VALUES(@ErrorId,@ErrorMessage ,@CreatedOn); SELECT CAST(SCOPE_IDENTITY() as int)";

        public const string InsertUserLoginHistory = @"INSERT INTO [dbo].[UserLoginHistory]([UserID],[UserAgent],[ClientIp],[LoginDate])
                                              VALUES(@UserID,@UserAgent ,@ClientIp,@LoginDate); SELECT CAST(SCOPE_IDENTITY() as int)";
        #endregion
        #region Select Queries
        public const string SelectUsersByUsername = "SELECT * from Health.dbo.Users where UserName = @UserName";
        public const string SelectRoles = "SELECT * from Health.dbo.Roles";
        public const string SelectRolesName = "SELECT * FROM Health.dbo.UserRoles INNER JOIN Health.dbo.Roles ON UserRoles.RoleID = Roles.Id where Name = @Name";
        
        #endregion

        #region Update Queries
        public const string UpdateUsersSuccessLoginByID = "UPDATE Health.dbo.Users SET IsLockedOut = @IsLockedOut,AccessFailedCount = @AccessFailedCount,LastLoginIp = @LastLoginIp,LastLoginDate = @LastLoginDate where Id = @Id";
        public const string UpdateUsersAccessFailedCountByID = "UPDATE Health.dbo.Users SET AccessFailedCount = @AccessFailedCount where Id = @Id";
        public const string IsLockedOut = "UPDATE Health.dbo.Users SET IsLockedOut = @IsLockedOut where Id = @Id";

        #endregion

    }
}
